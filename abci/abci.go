package abci

import (
	"encoding/binary"
	"log"
	"paradigm/tendercoin/store"

	"github.com/ethereum/go-ethereum/crypto"
	"github.com/tendermint/tendermint/abci/types"
)

type ABCI struct {
	types.BaseApplication
	store store.Store

	Size int64
}

func (abci *ABCI) CheckTx(tx []byte) types.ResponseCheckTx {
	if _, err := ParseTx(tx); err != nil {
		return types.ResponseCheckTx{Code: 400, Log: err.Error()}
	}

	return types.ResponseCheckTx{Code: types.CodeTypeOK}
}

func (abci *ABCI) DeliverTx(tx []byte) types.ResponseDeliverTx {
	obj, err := ParseTx(tx)
	if err != nil {
		return types.ResponseDeliverTx{Code: 400, Log: err.Error()}
	}

	abci.Size++

	switch t := obj.(type) {
	case *Transfer:
		log.Printf("new transfer %s", t)
		pub := t.PubKey()
		if false == t.Verify(pub) {
			return types.ResponseDeliverTx{Code: 400, Log: "Invalid signature verification"}
		}

		addr := crypto.PubkeyToAddress(*pub).Hex()

		if err := abci.store.Transfer(addr, t.Address, store.Amount(t.Amount)); err != nil {
			return types.ResponseDeliverTx{Code: 500, Log: err.Error()}
		}
	}

	return types.ResponseDeliverTx{}
}

func (abci *ABCI) Commit() types.ResponseCommit {
	appHash := make([]byte, 8)
	binary.PutVarint(appHash, abci.Size)
	return types.ResponseCommit{Data: appHash}
}

func (abci *ABCI) Query(req types.RequestQuery) types.ResponseQuery {
	switch req.Path {
	case "/balance":
		return abci.balance(&req)
	default:
		return types.ResponseQuery{Code: 404}
	}

	return types.ResponseQuery{Code: 0}
}

func (abci *ABCI) balance(req *types.RequestQuery) types.ResponseQuery {
	addr := string(req.Data)
	bln, err := abci.store.Balance(addr)
	if err != nil {
		return types.ResponseQuery{Code: 500, Info: err.Error()}
	}

	return types.ResponseQuery{Value: bln.Bytes(), Key: req.Data}
}

func (abci *ABCI) BeginBlock(req types.RequestBeginBlock) types.ResponseBeginBlock {
	log.Printf("BeginBlock: %x", req.GetHash())
	return types.ResponseBeginBlock{}
}

func (abci *ABCI) InitChain(req types.RequestInitChain) types.ResponseInitChain {
	log.Printf("InitChain: %+v", req.GetChainId())
	return types.ResponseInitChain{}
}

func New(store store.Store) *ABCI {
	return &ABCI{store: store}
}
