package abci

import (
	"encoding/base64"
	"testing"

	"github.com/ethereum/go-ethereum/crypto"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestTransferSignature(t *testing.T) {
	key1, _ := crypto.GenerateKey()
	key2, _ := crypto.GenerateKey()

	tr, err := NewTransfer(key1, crypto.PubkeyToAddress(key2.PublicKey), 100)
	require.NoError(t, err)

	assert.True(t, tr.Verify(&key1.PublicKey))
}

func TestCodec(t *testing.T) {
	key1, _ := crypto.GenerateKey()
	key2, _ := crypto.GenerateKey()

	tr, err := NewTransfer(key1, crypto.PubkeyToAddress(key2.PublicKey), 100)
	require.NoError(t, err)

	enc, err := EncodeTransfer(tr)
	require.NoError(t, err)

	decTr, err := DecodeTransfer(enc)
	require.NoError(t, err)

	assert.Equal(t, tr, decTr)
}

func TestParseTx(t *testing.T) {
	t.Run("Transfer", func(t *testing.T) {
		v := `{"op": "transfer", "body": {"pubkey": "pubkey", "address": "addr", "sig": "sig", "amount": 100}}`
		b64 := base64.StdEncoding.EncodeToString([]byte(v))

		op, err := ParseTx([]byte(b64))
		require.NoError(t, err)

		assert.Equal(t, &Transfer{Pub: "pubkey", Address: "addr", Sig: "sig", Amount: 100}, op)
	})

	t.Run("UnknownOp", func(t *testing.T) {
		v := `{"op": "xxx", "body": {}}`
		b64 := base64.StdEncoding.EncodeToString([]byte(v))

		_, err := ParseTx([]byte(b64))
		require.Error(t, err)
	})
}
