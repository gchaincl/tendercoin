package abci

import (
	"encoding/base64"
	"log"
	"paradigm/tendercoin/store"
	"paradigm/tendercoin/store/memdb"
	"testing"

	"github.com/ethereum/go-ethereum/crypto"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/tendermint/tendermint/abci/types"
)

func TestABCI(t *testing.T) {
	privKey, err := crypto.GenerateKey()
	require.NoError(t, err)

	addr := crypto.PubkeyToAddress(privKey.PublicKey)
	log.Printf("ADDR: %s", addr.Hex())
	amount := store.Amount(100)

	mem := memdb.New()
	mem.Fund(addr.Hex(), amount)
	app := New(mem)

	t.Run("Query", func(t *testing.T) {
		resp := app.Query(types.RequestQuery{
			Path: "/balance",
			Data: addr.Bytes(),
		})

		assert.EqualValues(t, 0, resp.Code)
		assert.Equal(t, amount.Bytes(), resp.Value)
	})

	t.Run("DeliverTx", func(t *testing.T) {
		prv, err := crypto.GenerateKey()
		require.NoError(t, err)

		tr, err := NewTransfer(privKey, crypto.PubkeyToAddress(prv.PublicKey), 10)
		require.NoError(t, err)

		b64, err := EncodeTransfer(tr)
		require.NoError(t, err)

		resp := app.DeliverTx(b64)
		require.EqualValues(t, 0, resp.Code, resp.Log)

		bln, err := mem.Balance(addr.Hex())
		require.NoError(t, err)

		assert.Equal(t, store.Amount(90), bln)
	})

	t.Run("CheckTx", func(t *testing.T) {
		resp := app.CheckTx([]byte("invalid tx"))
		assert.EqualValues(t, 400, resp.Code)

		v := `{"op": "transfer", "body": {"from": "addr", "to": "addr", "amount": 10}}`

		b64 := base64.StdEncoding.EncodeToString([]byte(v))
		resp = app.CheckTx([]byte(b64))
		assert.EqualValues(t, 0, resp.Code)
	})
}
