# Tendercoin
Tendercoind is my attempt to use Tendermint

# Install
Tendercoin uses `go-1.11` experimental modules so you'll need to enable them
```bash
export GO111MODULE=on
```

Get the code and build
```
git clone git@gitlab.com:gchaincl/tendercoin.git
cd tendercoin
go build .
```

# Test
```bash
go test ./... -race
```

# Run
First of all, run the ABCI.
The output is important because that's the initial key we'll use to do the initial transfer.
```bash
$ ./tendercoin abci
Using keypair:
 > PRIV: 0xb5b1d76584f0172e959b34e85debe5d1e61ac9a7e3352030dec144dbf3259f5b
 > PUB:  0x04957ef30381226c696919d4d67e1d38974ea2a1a14c0eda4804e8e795e7c734b57bd79c0e539d4dae775594e825dc38f3781fbbc9c272a3ea6cf4a6506578cb5f
 > ADDR: 0x79e8F02cB0D49D079657621939daeF021a60481C
Address: 0x79e8F02cB0D49D079657621939daeF021a60481C funded with +100
```

Once ABCI server is running, you can run a tendermint node
```bash
tendermint node --consensus.create_empty_blocks=false
```

If you need to restart the blockchain data run:
```bash
tendermint unsafe_reset_all
```
# Experiment

These are the commands and its output of a sample usage of the coins
```bash
# Check the balance:
$ ./tendercoin balance --addr 0x79e8F02cB0D49D079657621939daeF021a60481C # This is our initial account's address (see above)
```

```bash
# Transfer, first create a key to have a valid receiver address
$ ./tendercoin gen
Keys:
 Private: 0x909ba8754a67e8fdb027f8c7dcf2224ea5e9324adb3f322c071a5af3d73eb6ce
 Public:  0x044aced23813e5937fb7f1d37e7ccb5b7f1311a76450158ffcd63d5d6925d276b1e96383b8928a9c81801e2fca692e44e18f622c33ea56578370c7ab79e3dbac70
 Address: 0x7306F42ed41Cc6bee16F73A249Bb420d4d5c98f0
```

```bash
# Do the transfer from the initial funded account. `-addr` is the public key generated above
./tendercoin transfer \
  -priv-key 0xb5b1d76584f0172e959b34e85debe5d1e61ac9a7e3352030dec144dbf3259f5b \
  -addr 0x7306F42ed41Cc6bee16F73A249Bb420d4d5c98f0 \
  -amount=10
  
# And check the new balances
./tendercoin balance -addr 0x79e8F02cB0D49D079657621939daeF021a60481C
./tendercoin balance -addr 0x7306F42ed41Cc6bee16F73A249Bb420d4d5c98f0
```
