package tests

import (
	"paradigm/tendercoin/store"
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

type StoreSuite struct {
	suite.Suite
	store store.Store
}

func (s *StoreSuite) TestBalance() {
	bln, err := s.store.Balance("addr1")
	s.Require().NoError(err)
	s.Assert().EqualValues(100, bln)

	bln, err = s.store.Balance("unknown")
	s.Require().NoError(err)
	s.Assert().EqualValues(0, bln)
}

func (s *StoreSuite) TestTransfer() {
	amount := store.Amount(25)
	s.Require().NoError(
		s.store.Transfer("addr1", "addr2", amount),
	)

	bln1, _ := s.store.Balance("addr1")
	bln2, _ := s.store.Balance("addr2")

	s.Assert().True(store.Amount(100)-amount == bln1)
	s.Assert().True(store.Amount(0)+amount == bln2)
}

func Run(t *testing.T, store store.Store) {
	require.NoError(t,
		store.Fund("addr1", 100),
	)

	suite.Run(t, &StoreSuite{store: store})
}
