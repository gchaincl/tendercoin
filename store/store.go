package store

import (
	"errors"
	"fmt"
)

var (
	ErrNotEnoughFunds = errors.New("Not enough funds")
)

type Amount uint64

func (a Amount) Bytes() []byte {
	return []byte(fmt.Sprintf("%d", a))
}

type Store interface {
	Fund(string, Amount) error
	Balance(string) (Amount, error)
	Transfer(from, to string, amount Amount) error
}
