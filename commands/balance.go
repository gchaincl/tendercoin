package commands

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"gopkg.in/urfave/cli.v1"
)

type Balance struct {
}

func (cmd *Balance) Command() cli.Command {
	return cli.Command{
		Name:   "balance",
		Usage:  "Balance checks the current balance for a given --addr",
		Action: cmd.action,
		Flags: []cli.Flag{
			cli.StringFlag{Name: "addr"},
		},
	}
}

func (cmd *Balance) action(c *cli.Context) error {
	addr := c.String("addr")
	if addr == "" {
		return errors.New("addr can't be empty")
	}

	if !strings.HasPrefix(addr, "0x") {
		addr = "0x" + addr
	}

	url := fmt.Sprintf(`http://localhost:26657/abci_query?path="/balance"&data="%s"`, addr)
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	var jsonRPC struct {
		Result struct {
			Response struct {
				Key   []byte `json:"key"`
				Value []byte `json:"value"`
			} `json:"response"`
		} `json:"result"`
	}

	if err := json.NewDecoder(resp.Body).Decode(&jsonRPC); err != nil {
		return err
	}

	fmt.Printf("Balance for key '%s' is: %s",
		string(jsonRPC.Result.Response.Key),
		string(jsonRPC.Result.Response.Value),
	)

	return nil
}
