package commands

import (
	"fmt"
	"log"
	"paradigm/tendercoin/abci"
	"paradigm/tendercoin/store/memdb"

	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/tendermint/tendermint/abci/server"
	"github.com/tendermint/tendermint/libs/common"
	"gopkg.in/urfave/cli.v1"
)

const defaultRootKey = "b5b1d76584f0172e959b34e85debe5d1e61ac9a7e3352030dec144dbf3259f5b"

type ABCI struct {
	PrivKey string
}

func (cmd *ABCI) Command() cli.Command {
	return cli.Command{
		Name:   "abci",
		Usage:  "Starts the tendermint ABCI server",
		Action: cmd.action,
		Flags: []cli.Flag{
			cli.StringFlag{Name: "priv-key", Value: defaultRootKey},
			cli.IntFlag{Name: "amount", Value: 100},
		},
	}
}

func (cmd *ABCI) action(c *cli.Context) error {
	privKey, err := crypto.HexToECDSA(c.String("priv-key"))
	if err != nil {
		return fmt.Errorf("Parsing private key: %v", err)
	}
	pubKeyHex := hexutil.Encode(crypto.FromECDSAPub(&privKey.PublicKey))

	addr := crypto.PubkeyToAddress(privKey.PublicKey).Hex()
	fmt.Printf("Using keypair:\n > PRIV: 0x%s\n > PUB:  %+v\n > ADDR: %s\n", privKey.D.Text(16), pubKeyHex, addr)

	store := memdb.New()
	store.Fund(addr, 100)
	fmt.Printf("Address: %s funded with +%d\n", addr, 100)

	srv, err := server.NewServer("tcp://0.0.0.0:26658", "socket", abci.New(store))
	if err != nil {
		log.Fatal(err)
	}

	if err := srv.Start(); err != nil {
		log.Fatal(err)
	}

	common.TrapSignal(func() { srv.Stop() })
	return nil
}
