package commands

import (
	"fmt"

	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
	"gopkg.in/urfave/cli.v1"
)

type Generate struct {
}

func (cmd *Generate) Command() cli.Command {
	return cli.Command{
		Name:    "generate",
		Aliases: []string{"gen"},
		Usage:   "Generates a key pair",
		Action:  cmd.action,
	}
}

func (cmd *Generate) action(c *cli.Context) error {
	privKey, err := crypto.GenerateKey()
	if err != nil {
		return err
	}

	fmt.Printf("Keys:\n")
	fmt.Printf(" Private: 0x%s\n", privKey.D.Text(16))
	fmt.Printf(" Public:  %s\n", hexutil.Encode(crypto.FromECDSAPub(&privKey.PublicKey)))
	fmt.Printf(" Address: %s\n", crypto.PubkeyToAddress(privKey.PublicKey).Hex())
	return nil
}
